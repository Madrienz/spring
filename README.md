# Spring

Для настройки подключений к базам данных использовать:
*  application-prod.properties для PostgreSQL
*  application-local.properties для H2.

Для работы с PostgreSQL нужна база данных с названием songs. Для заполнения базы
используются sql скрипты в папке resources.

Профиль меняется в web.xml

Сервер Jetty создается в http://localhost:3333/song-system. Запускать программу
с параметрами clean package jetty:run.

REST endpoints:
*  GET http://localhost:3333/song-system/songs          получить список песен.
*  GET http://localhost:3333/song-system/songs/{id}     получить песню по id.
*  PUT http://localhost:3333/song-system/songs          изменить песню.
*  DELETE http://localhost:3333/song-system/songs/{id}  удалить песню по id.
*  POST http://localhost:3333/song-system/songs         создать песню.

Также имеется Postman-коллекция с необходимыми запросами.