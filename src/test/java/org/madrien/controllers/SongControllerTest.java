package org.madrien.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.madrien.TestObjectsContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
@ActiveProfiles("local")
@WebAppConfiguration
public class SongControllerTest {

    private static final int EXISTING_ID = 3;
    private static final int NON_EXISTING_ID = 10;

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ObjectMapper objectMapper = new ObjectMapper();

    private TestObjectsContainer testObjectsContainer = new TestObjectsContainer();

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getAllSongs() throws Exception {
        mockMvc.perform(get("/songs"))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    public void findSongById() throws Exception {
        mockMvc.perform(get("/songs/" + EXISTING_ID))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(EXISTING_ID)));
    }

    @Test
    public void createSong() throws Exception {
        mockMvc.perform(post("/songs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(testObjectsContainer.getNewSong())))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void updateSong() throws Exception {
        mockMvc.perform(put("/songs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(testObjectsContainer.getSong())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(testObjectsContainer.getSONG_ID().intValue())))
                .andExpect(jsonPath("$.name", is(testObjectsContainer.getSong().getName())));
    }

    @Test
    public void deleteSong() throws Exception {
        mockMvc.perform(delete("/songs/" + EXISTING_ID))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void songNotFound() throws Exception {
        mockMvc.perform(get("/songs/" + NON_EXISTING_ID))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void songIsNotCreated() throws Exception {
        mockMvc.perform(post("/songs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(testObjectsContainer.getSong())))
                .andDo(print())
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void songIsNotUpdated() throws Exception {
        mockMvc.perform(put("/songs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(testObjectsContainer.getNewSong())))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void songIsNotDeleted() throws Exception {
        mockMvc.perform(delete("/songs/" + NON_EXISTING_ID))
                .andDo(print())
                .andExpect(status().isNoContent());
    }
}