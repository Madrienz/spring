package org.madrien;

import org.madrien.entities.Album;
import org.madrien.entities.Band;
import org.madrien.entities.Song;

import java.util.Arrays;
import java.util.List;

public class TestObjectsContainer {

    private static final Long SONG_ID = 3L;
    private static final Long ALBUM_ID = 1L;
    private static final String ALBUM_NAME = "Art of Doubt";
    private static final int RELEASE_YEAR = 2018;

    private static final Long BAND_ID = 5L;
    private static final String BAND_NAME = "Metric";
    private static final int FOUNDATION_YEAR = 1998;

    private Song song = new Song(SONG_ID, "Test Song");
    {
        song.setAlbum(new Album(ALBUM_ID, ALBUM_NAME, RELEASE_YEAR));
        song.setBand(new Band(BAND_ID, BAND_NAME, FOUNDATION_YEAR));
        song.setDuration(310);
    }

    private Song newSong = new Song();
    {
        newSong.setName("Song Without ID");
        newSong.setAlbum(new Album(ALBUM_ID, ALBUM_NAME, RELEASE_YEAR));
        newSong.setBand(new Band(BAND_ID, BAND_NAME, FOUNDATION_YEAR));
        newSong.setDuration(310);
    }

    private List<Song> list = Arrays.asList(
            new Song(1L, "Test Song"),
            new Song(2L, "Test Song #2"));

    public Song getSong() {
        return song;
    }

    public Song getNewSong() {
        return newSong;
    }

    public List<Song> getList() {
        return list;
    }

    public Long getSONG_ID() {
        return SONG_ID;
    }
}
