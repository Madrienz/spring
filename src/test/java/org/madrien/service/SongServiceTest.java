package org.madrien.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.madrien.TestObjectsContainer;
import org.madrien.dao.implementations.SongDAOImpl;
import org.madrien.entities.Song;
import org.madrien.exceptions.PostHasIdException;
import org.madrien.exceptions.SongNotFoundException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring-config.xml")
@ActiveProfiles("local")
public class SongServiceTest {

    private static final Long EXISTING_ID = 3L;
    private static final Long NON_EXISTING_ID = 10L;

    @Mock
    private SongDAOImpl songDAO;

    private TestObjectsContainer testObjectsContainer = new TestObjectsContainer();

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllSongs() {
        List<Song> testList = testObjectsContainer.getList();
        when(songDAO.findAll()).thenReturn(testList);

        assertEquals(testList.get(0).getId(), songDAO.findAll().get(0).getId());
        assertEquals(testList.get(0).getName(), songDAO.findAll().get(0).getName());

        verify(songDAO, times(2)).findAll();
    }

    @Test
    public void findBy() {
        Song testSong = testObjectsContainer.getSong();
        when(songDAO.findBy(EXISTING_ID)).thenReturn(testSong);

        assertEquals(testSong.getId(), songDAO.findBy(EXISTING_ID).getId());
        assertEquals(testSong.getName(), songDAO.findBy(EXISTING_ID).getName());

        verify(songDAO, times(2)).findBy(EXISTING_ID);
    }

    @Test
    public void save() {
        Song testSong = testObjectsContainer.getNewSong();
        when(songDAO.save(testSong)).thenReturn(testSong);

        assertEquals(testSong.getId(), songDAO.save(testSong).getId());
        assertEquals(testSong.getName(), songDAO.save(testSong).getName());

        verify(songDAO, times(2)).save(testSong);
    }

    @Test
    public void update() {
        Song testSong = testObjectsContainer.getSong();
        when(songDAO.update(testSong)).thenReturn(testSong);

        assertEquals(testSong.getId(), songDAO.update(testSong).getId());
        assertEquals(testSong.getName(), songDAO.update(testSong).getName());

        verify(songDAO, times(2)).update(testSong);
    }

    @Test
    public void delete() {
        doNothing().when(songDAO).delete(EXISTING_ID);
        songDAO.delete(EXISTING_ID);
        verify(songDAO, times(1)).delete(EXISTING_ID);
    }

    @Test(expected = SongNotFoundException.class)
    public void songNotFound() {
        when(songDAO.findBy(NON_EXISTING_ID)).thenThrow(new SongNotFoundException());
        songDAO.findBy(NON_EXISTING_ID);
    }

    @Test(expected = PostHasIdException.class)
    public void songNotCreated() {
        Song testSong = testObjectsContainer.getSong();
        when(songDAO.save(testSong)).thenThrow(new PostHasIdException());
        songDAO.save(testSong);
    }

    @Test(expected = SongNotFoundException.class)
    public void songNotUpdated() {
        Song testSong = testObjectsContainer.getNewSong();
        when(songDAO.update(testSong)).thenThrow(new SongNotFoundException());
        songDAO.update(testSong);
    }

    @Test
    public void songNotDeleted() {
        doNothing().when(songDAO).delete(NON_EXISTING_ID);
        songDAO.delete(NON_EXISTING_ID);
        verify(songDAO, times(1)).delete(NON_EXISTING_ID);
    }
}