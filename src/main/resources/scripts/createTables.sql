CREATE TABLE IF NOT EXISTS albums (
    id SERIAL,
    name VARCHAR(100),
    releaseYear INT,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS bands (
    id SERIAL,
    name VARCHAR(100),
    foundationYear INT,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS songs (
    id SERIAL,
    name VARCHAR(100),
    album_id INT,
    band_id INT,
    duration INT,
	FOREIGN KEY(album_id) REFERENCES albums (id), 
    FOREIGN KEY(band_id) REFERENCES bands (id), 
    PRIMARY KEY (id)
);