INSERT INTO albums 
    VALUES (DEFAULT, 'Art of Doubt', 2018), 
            (DEFAULT, 'Humbug', 2009), 
            (DEFAULT, 'Demon Days', 2005), 
            (DEFAULT, 'Manipulator', 2007), 
            (DEFAULT, 'Money Shot', 2016);
			
INSERT INTO bands 
    VALUES (DEFAULT, 'Puscifer', 2007), 
            (DEFAULT, 'Arctic Monkeys', 2002), 
            (DEFAULT, 'Gorillaz', 1998), 
            (DEFAULT, 'The Fall of Troy', 2002), 
            (DEFAULT, 'Metric', 1998);
			
INSERT INTO songs 
    VALUES (DEFAULT, 'The Arsonist', 5, 1, 152), 
            (DEFAULT, 'Fire And The Thud', 2, 2, 237), 
            (DEFAULT, 'Risk', 1, 5, 324), 
            (DEFAULT, 'Sledgehammer', 4, 4, 361), 
            (DEFAULT, 'Feel Good Inc', 3, 3, 222);