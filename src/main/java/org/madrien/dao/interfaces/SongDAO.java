package org.madrien.dao.interfaces;

import org.madrien.entities.Song;

import java.util.List;

public interface SongDAO {
    List<Song> findAll();
    Song findBy(Long id);
    Song save (Song song);
    Song update(Song song);
    void delete(Long id);
}
