package org.madrien.dao.implementations;

import org.madrien.dao.interfaces.SongDAO;
import org.madrien.entities.Song;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class SongDAOImpl implements SongDAO {

    private static final String FIND_ALL_SONGS = "SELECT * FROM songs;";

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Song> findAll() {
        return em.createNativeQuery(FIND_ALL_SONGS).getResultList();
    }

    @Override
    public Song findBy(Long id) {
        return em.find(Song.class, id);
    }

    @Override
    public Song save(Song song) {
        em.persist(song);
        return song;
    }

    @Override
    public Song update(Song song) {
        return em.merge(song);
    }

    @Override
    public void delete(Long id) {
        Song song = em.find(Song.class, id);
        if(song != null) {
            em.remove(song);
        }
    }
}
