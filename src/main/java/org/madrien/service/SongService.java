package org.madrien.service;

import org.madrien.dao.interfaces.SongDAO;
import org.madrien.entities.Song;
import org.madrien.exceptions.PostHasIdException;
import org.madrien.exceptions.SongNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;

import javax.validation.Valid;
import java.util.List;

@Service
public class SongService {

    private final SongDAO dao;

    public SongService(SongDAO dao) {
        this.dao = dao;
    }

    public List<Song> getAllSongs() {
        return dao.findAll();
    }

    public Song findBy(Long id) {
        Song song = dao.findBy(id);
        if(song == null) {
            throw new SongNotFoundException(id);
        }
        return song;
    }

    public Song save(@Valid Song song, BindingResult result) throws BindException {
        if(result.hasErrors()) {
            throw new BindException(result);
        }
        if(song.getId() != null) {
            throw new PostHasIdException();
        }
        return dao.save(song);
    }

    public Song update(@Valid Song song, BindingResult result) throws BindException {
        if(result.hasErrors()) {
            throw new BindException(result);
        }
        if(song.getId() == null) {
            throw new SongNotFoundException();
        }
        findBy(song.getId());
        return dao.update(song);
    }

    public void delete(Long id) {
        dao.delete(id);
    }
}
