package org.madrien.controllers;

import org.madrien.entities.Song;
import org.madrien.service.SongService;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/songs")
public class SongController {

    private final SongService service;

    public SongController(SongService service) {
        this.service = service;
    }

    @GetMapping
    public List<Song> getAllSongs() {
        return service.getAllSongs();
    }

    @GetMapping("/{id}")
    public Song findSongById(@PathVariable("id") Long id) {
        return service.findBy(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Song createSong(@RequestBody Song song, BindingResult result) throws BindException {
        return service.save(song, result);
    }

    @PutMapping
    public Song updateSong(@RequestBody Song song, BindingResult result) throws BindException {
        return service.update(song, result);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteSong(@PathVariable("id") Long id) {
        service.delete(id);
    }
}
