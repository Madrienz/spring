package org.madrien.exceptions;

public class SongNotFoundException extends RuntimeException {

    public SongNotFoundException() {
        super("No songs found.");
    }

    public SongNotFoundException(Long id) {
        super("Song id not found: " + id);
    }
}
