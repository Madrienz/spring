package org.madrien.exceptions;

public class PostHasIdException extends RuntimeException {

    public PostHasIdException() {
        super("Can not POST entity with non-null 'id'.");
    }
}
